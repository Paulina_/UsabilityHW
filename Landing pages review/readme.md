# Landing pages обзор 

### Parallax effect 

##### [1. Lunita](http://lunita.ca/)

Традиционный итальянский бар

##### [2. Alessio Santangelo](http://www.alessiosantangelo.it/)

Визитка frontend-разработчика  

##### [3. Pedicab](http://www.spokespedicabs.com/)

Велорикши для свадьбы, дня рожждения или просто поездок

##### [4. Firewatchgame](http://www.firewatchgame.com/)

Landing page приключенческой видеоигры от первого лица, разработанной компанией Campo Santo

##### [5. Beckett](https://www.beckett.de/)

Немецкая компания, предоставляющая услуги в области веб-дизайна, онлайн-маркетинга, печатной рекламы и графического дизайна
