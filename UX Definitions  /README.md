# Аннотации к видео о UX дизайне
### [1.	Что такое UX дизайн, от UX Mastery](https://www.youtube.com/watch?v=PPBVmG_FYyQ)

UX дизайн - это дизайн за пределами визуального дизайна, т.к. графический дизайн составляет лишь малую его часть. 

В понимании компании UX Mastery UX дизайнер проектирует взаимодействия между пользователем и продуктом, а также он постоянно задает тонны вопросов:

![UX designer](img/first.png)

Несколько причин, по которым следует узнать о UX больше (по мнению специалиста компании UX Mastery):
  - Вы, вероятно, уже делаете что-то из этого.
  - Проектирование, ориентиованное на пользователя, является процессом, а значит это практически научно.
  - Это не сложно (особенно для технарей).
  - Это весело. Быть UX дизайнером - интересно, а также эта работа хорошо оплачивается.

### [2. Что такое UX, от Oakwood](https://www.youtube.com/watch?v=PicG4jzJHfw)
Задача компании Oakwood - создание продуктов, пользование которыми приносит удовольствие, а их цель - придать уверенности, уменьшить разочарование и путанницу.

UX - это общий опыт пользования продуктом, сайтом или приложением, показывающий насколько он легок и приятен к использованию.
UX применим ко многим аспектам нашей жизни. 

Вот несколько примеров того неудобное использование приводит к негативному опыту:

![negative experience](img/second.png)

### [3. Что такое UX дизайн, от artslondonllc](https://www.youtube.com/watch?v=zyN5dQvRT7A)
UX дизайн призван облегчать пользование продуктом.

UX дизайнер прослеживает взаимодействие пользователя с интерфейсом: кто пользуется, как и почему. Эта информация позволяет UX дизайнеру улучшать продукт, сохраняя его функциональность.

В частности, это позволяет избегать перегруженности, повышать эффективность, более удобно группировать элементы. Если не дать этого, тогда процесс использования продукта запутывает и раздражает:

![elements](img/third.png)

### [4. UX дизайн для разработчиков мобильных приложений, от Nagore Vidan](https://www.youtube.com/watch?v=4wNOOx5a3jw)
UX - это опыт взаимодействия. Чувстава человека при пользовании приложением.

UX обращает внимание на следующее: удобство, доступность, маркетинг, производительность программы и полезность:

![aspects](img/fourth.png)

Сотрудники Nagore Vidan UX проводят аналогию с человеком: UX- это органы человека, благодаря которым он живет, а UI - это косметика.

### [5. Примеры UX/UI дизайна от Forty](https://www.youtube.com/watch?v=9zKkGfz1VsY)

Ключевые шаги работы с UI и UX дизайном (примеры по версии Forty):
  - Начиинать работать с лююдей, котрые будут пользоваться продуктом, а не с его характеристик.
  - Осмыслить весь продукт целиком, а не отдельные его части.
  - Использовать дизайна, для того, чтобы акцентировать внимание пользователя на определенных деталях.
  - Искать способы упростить интерфейс.
  - Внедрять бренд в дизайн.
  - Акцент на вербальной стороне проекта.