# Гештальт - принципы в дизайне интерфейсов

### Близость

##### [1. Your Floral Home Decor](https://www.pinterest.ru/pin/379428337351519081/)
![proximity one](img/proximity.jpg)

##### [2. Dashboard Design](https://dribbble.com/shots/5323473-Dashboard-Design)
![proximity two](img/proximity2.png)

##### [3. Employee Dashboard Design](https://dribbble.com/shots/5278936-Employee-Dashboard-Design)
![proximity three](img/proximity3.png)

### Общая область

##### [1. How It Works Freebies](https://dribbble.com/shots/4326672-How-It-Works-Freebies)
![general proximity_one](img/general_proximity2.png)

##### [2. Matin M](https://www.pinterest.ru/pin/739716307520668163/)
![general proximity_two](img/general_proximity.jpg)

##### [3. The Kuusamo area lakes](https://www.pinterest.ru/pin/514677063661282030/)
![general proximity_three](img/general_proximity3.jpg)


### Схожесть

##### [1. Screen time activity dashboard](https://dribbble.com/shots/5291181-Daily-UI-066)
![similarity one](img/similarity.png)

##### [2. Summer Coffeedrinks](https://www.pinterest.ru/pin/864339353460592936/)
![similarity two](img/similarity2.jpg)

##### [3. Streaming study project](https://dribbble.com/shots/4706162-Streaming)
![similarity three](img/similarity3.png)


### Замкнутость

##### [1. Weather App UI](https://www.pinterest.ru/pin/411657222183083512/)
![isolation one](img/isolation.jpg)

##### [2. Virtual Fitness Coach](https://www.pinterest.ru/pin/374995106462826073/)
![isolation two](img/isolation2.jpg)

##### [3. Анатомия веб-дизайнера](https://www.pinterest.ru/pin/806707351972217688/)
![isolation three](img/isolation3.jpg)


### Симметрия

##### [1. Interface](https://www.pinterest.ru/pin/414190496973274647/)
![symmetry one](img/symmetry.jpg)

##### [2. Giorgios](https://www.pinterest.ru/pin/299137600240231875/)
![symmetry two](img/symmetry3.jpg)

##### [3. Screen time activity dashboard](https://dribbble.com/shots/5291181-Daily-UI-066)
![symmetry three](img/similarity.png)


### Продолжение

##### [1. Website Design](https://www.pinterest.ru/pin/507429083000146054/)
![continuation one](img/continuation.jpg)

##### [2. Website Design](https://www.pinterest.ru/pin/513269688757217254/)
![continuation two](img/continuation2.jpg)

##### [3. Pizza house](https://www.pinterest.ru/pin/102738435222192322/)
![continuation three](img/continuation3.jpg)


### Общая судьба
##### [1. Hydro Mobil Service](https://www.pinterest.ru/pin/20829217010951008/)
![common fate one](img/common_fate.jpg)

##### [2. Деньги под залог авто](https://www.pinterest.ru/pin/796363146583296339/)
![common fate two](img/common_fate2.jpg)

##### [3. Book your winter sport](https://www.pinterest.ru/pin/450219293993101066/)
![common fate three](img/common_fate3.jpg)
